;This file will be executed next to the application bundle image
;I.e. current directory will contain folder KOSOverlay with application files
[Setup]
AppId={{KOSOverlayLauncher}}
AppName=KOSOverlay
AppVersion=1.2
AppVerName=KOSOverlay 1.2
AppPublisher=Louis Burton
AppComments=
AppCopyright=
;AppPublisherURL=http://java.com/
;AppSupportURL=http://java.com/
;AppUpdatesURL=http://java.com/
DefaultDirName={localappdata}\KOSOverlay
DisableStartupPrompt=Yes
DisableDirPage=Yes
;DisableProgramGroupPage=Yes
DisableReadyPage=Yes
DisableFinishedPage=Yes
DisableWelcomePage=Yes
DefaultGroupName=KOSOverlay
;Optional License
LicenseFile=C:\DEV\Personal\repos\kosoverlay\LICENSE.TXT
;WinXP or above
MinVersion=0,5.1
OutputBaseFilename=KOSOverlay-1.2
Compression=lzma
SolidCompression=yes
PrivilegesRequired=lowest
SetupIconFile=KOSOverlay\KOSOverlay.ico
UninstallDisplayIcon={app}\KOSOverlay.ico
UninstallDisplayName=KOSOverlay
WizardImageStretch=No
WizardSmallImageFile=KOSOverlay-setup-icon.bmp

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "KOSOverlay\KOSOverlay.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "KOSOverlay\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs

[Icons]
Name: "{group}\KOSOverlay"; Filename: "{app}\KOSOverlay.exe"; IconFilename: "{app}\KOSOverlay.ico"; Check: returnTrue()
Name: "{commondesktop}\KOSOverlay"; Filename: "{app}\KOSOverlay.exe";  IconFilename: "{app}\KOSOverlay.ico"; Tasks: desktopicon

[Run]
Filename: "{app}\KOSOverlay.exe"; Description: "{cm:LaunchProgram,KOSOverlay}"; Flags: nowait postinstall skipifsilent

[Code]
function returnTrue(): Boolean;
begin
  Result := True;
end;

function returnFalse(): Boolean;
begin
  Result := False;
end;

function InitializeSetup(): Boolean;
begin
// Possible future improvements:
//   if version less or same => just launch app
//   if upgrade => check if same app is running and wait for it to exit
//   Add pack200/unpack200 support?
  Result := True;
end;
