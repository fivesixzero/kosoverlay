/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.view.fx;

import javafx.scene.image.Image;


/**
 * Holds constant values related to the style and display of the KOS Overlay
 *
 * @author Louis Burton
 */
public class UIConstants {

    public static final Image LOADING_IMAGE = new Image(UIConstants.class.getResourceAsStream("loading.gif"));
    public static final Image ERROR_IMAGE = new Image(UIConstants.class.getResourceAsStream("error.png"));
    public static final Image INFO_IMAGE = new Image(UIConstants.class.getResourceAsStream("info.png"));
    public static final String LOADING_STYLE = "loading";
    public static final String FAILED_STYLE = "failed";
    public static final String BLUE_STYLE = "blue";
    public static final String LIGHT_BLUE_STYLE = "lightblue";
    public static final String GREEN_STYLE = "green";
    public static final String KOS_STYLE = "kos";
    public static final String RED_STYLE = "red";
    public static final String ORANGE_STYLE = "orange";
    public static final String STANDINGS_STYLE = "standings";
    public static final String NEUTRAL_STYLE = "neutral";
    public static final String NEUTRAL_STANDINGS_STYLE = "neutral-standings";
    public static final String BASE_TEXT_STYLE = "text";
    public static final String RESULT_BUTTON_STYLE = "result-button";
    public static final long FADE_TRANSITION_DURATION = 3500L;

    public static final String[] OVERLAY_STYLES = new String[]{LOADING_STYLE, FAILED_STYLE, KOS_STYLE, RED_STYLE, ORANGE_STYLE, STANDINGS_STYLE, NEUTRAL_STYLE, NEUTRAL_STANDINGS_STYLE, LIGHT_BLUE_STYLE, BLUE_STYLE, GREEN_STYLE};
}