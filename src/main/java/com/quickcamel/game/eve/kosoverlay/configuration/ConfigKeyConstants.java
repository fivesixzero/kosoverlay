/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.configuration;

/**
 * Description: Keys used in the configuration of the KOS Overlay
 *
 * @author Louis Burton
 */
public class ConfigKeyConstants {

    public static final String ANCHORED = "anchored";
    public static final String POSITION_X = "position.x";
    public static final String POSITION_Y = "position.y";
    public static final String RESULTS_MAXIMUM = "results.maximum";
    public static final String THREADCOUNT = "threadcount";
    public static final String FADE_PAUSE_DURATION = "fade.pause.duration";
    public static final String HELP_SHOWN = "help.shown";
    public static final String PILOTINFO_URL = "pilotinfo.url";
    public static final String PILOTINFO_URL_POST_NEEDED = "pilotinfo.url.post.needed";
    public static final String PILOTINFO_URL_POST_PARAMS = "pilotinfo.url.post.params";
    public static final String PILOTINFO_URL_POST_EVEKILL = "pilotinfo.url.post.evekill.currentmonthlosses";
    public static final String API_CHARACTERNAME = "api.charactername";
    public static final String API_CHARACTERID_PREFIX = "api.characterid.";
    public static final String API_KEYID = "api.keyid";
    public static final String API_VCODE = "api.vcode";
    public static final String BATCHSERVICE_LARGEBATCH_NONEGATIVESTANDINGS = "batchservice.largebatch.nonegativestandings";
    public static final String BATCHSERVICE_LARGEBATCH_THRESHOLD = "batchservice.largebatch.threshold";
    public static final String BATCHSERVICE_LARGEBATCH_KOSCHECKALL = "batchservice.largebatch.koscheckall";
    public static final String CACHE_EXPIRY_MINUTES = "cache.expiry.minutes";
    public static final String OVERLAY_ALWAYSONTOP_DELAY_MS = "overlay.alwaysontop.delay.seconds";
    public static final String PLAY_KOS_SOUND = "play.kos.sound";
    public static final String PLAY_KOS_SOUND_ONCE = "play.kos.sound.once";
    public static final String CUSTOM_KOS_SOUND = "custom.kos.sound";
    public static final String BATCHSERVICE_ONLYCHECKNOSTANDINGS = "batchservice.onlychecknostandings";
    public static final String BATCHSERVICE_BATCHPRIMING = "batchservice.batchpriming";
    public static final String CVABATCHPRIMER_MAX = "cvabatchprimer.max";
    public static final String KOSCHECKER_CHECKLASTCORP = "koschecker.checklastcorp";
    public static final String STANDINGS_USEALLIANCE = "standings.usealliance";
    public static final String STANDINGS_USECORP = "standings.usecorp";
    public static final String STANDINGS_USEPERSONAL = "standings.usepersonal";
    public static final String KEYBOARD_CUSTOM_MODIFIERS = "keyboard.custom.modifiers";
    public static final String KEYBOARD_CUSTOM_KEY = "keyboard.custom.key";
    public static final String KEYBOARD_KILLKEY_ENABLED = "keyboard.killkey.enabled";
    public static final String KEYBOARD_KILLKEY_THREADDUMP = "keyboard.killkey.threaddump";


    /**
     * DEFAULTS
     */
    public static final String THREADCOUNT_DEFAULT = "20";
    public static final String FADE_PAUSE_DURATION_DEFAULT = "6000";
    public static final String BATCHSERVICE_LARGEBATCH_NONEGATIVESTANDINGS_DEFAULT = "false";
    public static final String BATCHSERVICE_LARGEBATCH_THRESHOLD_DEFAULT = "7";
    public static final String RESULTS_MAXIMUM_DEFAULT = "7";
    public static final String PILOTINFO_URL_DEFAULT = "https://zkillboard.com/character/<eveid>/";
    public static final String PILOTINFO_URL_POST_NEEDED_DEFAULT = "false";
    public static final String PILOTINFO_URL_POST_EVEKILL_DEFAULT = "true";
    public static final String CACHE_EXPIRY_MINUTES_DEFAULT = "60";
    public static final String OVERLAY_ALWAYSONTOP_DELAY_MS_DEFAULT = "-1";
    public static final String PLAY_KOS_SOUND_DEFAULT = "true";
    public static final String PLAY_KOS_SOUND_ONCE_DEFAULT = "false";
    public static final String BATCHSERVICE_ONLYCHECKNOSTANDINGS_DEFAULT = "false";
    public static final String BATCHSERVICE_BATCHPRIMING_DEFAULT = "true";
    public static final String BATCHSERVICE_LARGEBATCH_KOSCHECKALL_DEFAULT = "false";
    public static final String CVABATCHPRIMER_MAX_DEFAULT = "100";
    public static final String KOSCHECKER_CHECKLASTCORP_DEFAULT = "true";
    public static final String STANDINGS_USEALLIANCE_DEFAULT = "true";
    public static final String STANDINGS_USECORP_DEFAULT = "false";
    public static final String STANDINGS_USEPERSONAL_DEFAULT = "false";
    public static final String KEYBOARD_KILLKEY_ENABLED_DEFAULT = "false";
    public static final String KEYBOARD_KILLKEY_THREADDUMP_DEFAULT = "false";

}
