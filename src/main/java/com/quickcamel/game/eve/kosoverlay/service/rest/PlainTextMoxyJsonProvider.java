/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service.rest;

import org.glassfish.jersey.moxy.json.internal.ConfigurableMoxyJsonProvider;

import javax.inject.Singleton;
import javax.ws.rs.core.MediaType;

/**
 * Description: A JSOM MessageBodyReader that supports responses with plain/text Content-Type
 * Unfortunately, CVA's API always describes its response as plain/text despite having asked for JSON and receiving JSON
 *
 * @author Louis Burton
 */
@Singleton
public class PlainTextMoxyJsonProvider extends ConfigurableMoxyJsonProvider {

    protected boolean supportsMediaType(MediaType mediaType) {
        return mediaType.equals(MediaType.TEXT_PLAIN_TYPE) || super.supportsMediaType(mediaType);
    }
}
