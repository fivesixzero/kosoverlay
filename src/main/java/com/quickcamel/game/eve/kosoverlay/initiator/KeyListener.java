/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.initiator;

import com.quickcamel.game.eve.kosoverlay.KOSOverlayLauncher;
import com.quickcamel.game.eve.kosoverlay.configuration.*;
import com.quickcamel.game.eve.kosoverlay.service.IKOSBatchService;
import javafx.application.Platform;
import javafx.scene.input.Clipboard;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.jnativehook.NativeInputEvent;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.lang.management.*;
import java.util.Arrays;

/**
 * The key listener that starts the KOS checks
 *
 * @author Louis Burton
 */
public class KeyListener implements NativeKeyListener, IConfigChangeListener {

    private static final Logger m_logger = LoggerFactory.getLogger(KeyListener.class);

    @Inject
    private IKOSBatchService m_batchService;

    @Inject
    private IConfigManager m_configManager;

    private KOSOverlayLauncher m_launcher;

    private Clipboard m_clipboard;

    private long m_lastCopy = 0;

    private boolean m_killKeyEnabled = false;
    private long m_killKeyStart = 0;
    private int m_killKeyCount = 0;

    private int m_customKey = 0;
    private int m_customKeyModifiers = 0;


    @PostConstruct
    public void initialise() {
        notifyConfigChange();
        Platform.runLater(new Runnable() {
            public void run() {
                m_clipboard = Clipboard.getSystemClipboard();
            }
        });
    }

    @Override
    public void nativeKeyPressed(NativeKeyEvent nativeKeyEvent) {
        if ((nativeKeyEvent.getKeyCode() == NativeKeyEvent.VK_C || nativeKeyEvent.getKeyCode() == NativeKeyEvent.VK_X)
                && nativeKeyEvent.getModifiers() == NativeInputEvent.CTRL_MASK) {
            long now = System.currentTimeMillis();
            if (now - m_lastCopy > 1000) {
                m_lastCopy = now;
            }
            else {
                m_logger.debug(nativeKeyEvent.toString());
                m_lastCopy = 0;
                runFromClipboard();
            }
        }
        else if (m_killKeyEnabled && nativeKeyEvent.getKeyCode() == NativeKeyEvent.VK_ALT) {
            long now = System.currentTimeMillis();
            if (now - m_killKeyStart > 2000) {
                m_killKeyStart = now;
                m_killKeyCount = 0;
            }
            else {
                if (++m_killKeyCount > 2) {
                    // Kill after 4 quick presses
                    if (Boolean.valueOf(m_configManager.getValue(ConfigKeyConstants.KEYBOARD_KILLKEY_THREADDUMP,
                            ConfigKeyConstants.KEYBOARD_KILLKEY_THREADDUMP_DEFAULT))) {
                        try {
                            FileUtils.writeStringToFile(new File("logs/tdump-" + System.currentTimeMillis() + ".dump"), generateThreadDump());
                        }
                        catch (IOException e) {
                            m_logger.error("Unable to write thread dump", e);
                        }
                    }
                    try {
                        m_launcher.stop();
                    }
                    catch (Exception e) {
                        m_logger.error("Unable to stop", e);
                    }
                }
            }
        }
        if (m_customKey > 0
                && nativeKeyEvent.getKeyCode() == m_customKey
                && (m_customKeyModifiers == 0 || nativeKeyEvent.getModifiers() == m_customKeyModifiers)) {
            runFromClipboard();
        }
    }

    @Override
    public void nativeKeyReleased(NativeKeyEvent nativeKeyEvent) {
        if (nativeKeyEvent.getKeyCode() == NativeKeyEvent.VK_CONTROL && m_lastCopy > 0) {
            m_lastCopy = 0;
        }
    }

    @Override
    public void nativeKeyTyped(NativeKeyEvent nativeKeyEvent) {
        // do nothing
    }

    public void runFromClipboard() {
        Platform.runLater(new Runnable() {
            public void run() {
                String kosString = m_clipboard.getString();
                m_logger.debug(kosString);
                if (kosString != null) {
                    final String[] pilotNames = kosString.split("\n|  ");
                    m_batchService.startBatch(Arrays.asList(pilotNames));
                }
            }
        });
    }

    public void registerLauncher(KOSOverlayLauncher launcher) {
        m_launcher = launcher;
    }

    private String generateThreadDump() {
        final StringBuilder dump = new StringBuilder();
        final ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
        final ThreadInfo[] threadInfos = threadMXBean.getThreadInfo(threadMXBean.getAllThreadIds(), 100);
        for (ThreadInfo threadInfo : threadInfos) {
            dump.append('"');
            dump.append(threadInfo.getThreadName());
            dump.append("\" ");
            final Thread.State state = threadInfo.getThreadState();
            dump.append("\n   java.lang.Thread.State: ");
            dump.append(state);
            final StackTraceElement[] stackTraceElements = threadInfo.getStackTrace();
            for (final StackTraceElement stackTraceElement : stackTraceElements) {
                dump.append("\n        at ");
                dump.append(stackTraceElement);
            }
            dump.append("\n\n");
        }
        return dump.toString();
    }

    @Override
    public void notifyConfigChange() {
        m_killKeyEnabled = Boolean.valueOf(m_configManager.getValue(ConfigKeyConstants.KEYBOARD_KILLKEY_ENABLED, ConfigKeyConstants.KEYBOARD_KILLKEY_ENABLED_DEFAULT));
        if (!StringUtils.isEmpty(m_configManager.getValue(ConfigKeyConstants.KEYBOARD_CUSTOM_KEY))) {
            m_customKey = Integer.valueOf(m_configManager.getValue(ConfigKeyConstants.KEYBOARD_CUSTOM_KEY));
        }
        else {
            m_customKey = 0;
        }
        if (!StringUtils.isEmpty(m_configManager.getValue(ConfigKeyConstants.KEYBOARD_CUSTOM_MODIFIERS))) {
            m_customKeyModifiers = Integer.valueOf(m_configManager.getValue(ConfigKeyConstants.KEYBOARD_CUSTOM_MODIFIERS));
        }
        else {
            m_customKeyModifiers = 0;
        }
    }
}
