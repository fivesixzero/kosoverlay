/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service.rest;

import org.glassfish.jersey.filter.LoggingFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.List;
import java.util.Map;

/**
 * Jersey implementation of the IRestClient
 *
 * @author Louis Burton
 */
public class JerseyRestClient implements IRestClient {

    private static final Logger m_logger = LoggerFactory.getLogger(JerseyRestClient.class);

    private Client m_client;

    /**
     * Create a new instance of rest client
     *
     * @param clientConfig the underlying jersey client's configuration
     */
    public JerseyRestClient(Configuration clientConfig) {
        m_client = ClientBuilder.newClient(clientConfig);
        if (m_logger.isDebugEnabled()) {
            LoggingFilter loggingFilter = new LoggingFilter(java.util.logging.Logger.getLogger(LoggingFilter.class.getName()), true);
            m_client.register(loggingFilter);
        }
    }

    @Override
    public <T> T get(String path, Map<String, List<String>> queryParams, Map<String, String> headerParams,
                     Class<T> returnEntityType) throws RestClientException {
        WebTarget webResource = m_client.target(path);

        // Query Params
        if (queryParams != null) {
            for (Map.Entry<String, List<String>> queryParam : queryParams.entrySet()) {
                webResource = webResource.queryParam(queryParam.getKey(), queryParam.getValue() == null ? null : queryParam.getValue().toArray());
            }
        }
        Invocation.Builder webResourceRequestBuilder = webResource.request();
        // Header Params
        if (headerParams != null) {
            for (Map.Entry<String, String> headerEntry : headerParams.entrySet()) {
                webResourceRequestBuilder = webResourceRequestBuilder.header(headerEntry.getKey(), headerEntry.getValue());
            }
        }
        try {
            return webResourceRequestBuilder.get(returnEntityType);
        }
        catch (WebApplicationException e) {
            // if the information in this exception is ever needed, propagate to decoupled RestClientException
            throw new RestClientException(e);
        }
    }

    @Override
    public URI post(String path, Map<String, String> formParams) throws RestClientException {
        WebTarget webResource = m_client.target(path);

        // Form Params
        Form form = new Form();
        if (formParams != null) {
            for (Map.Entry<String, String> formParam : formParams.entrySet()) {
                form.param(formParam.getKey(), formParam.getValue());
            }
        }

        Invocation.Builder webResourceRequestBuilder = webResource.request();

        try {
            return webResourceRequestBuilder.post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE)).getLocation();
        }
        catch (WebApplicationException e) {
            // if the information in this exception is ever needed, propagate to decoupled RestClientException
            throw new RestClientException(e);
        }
    }
}
