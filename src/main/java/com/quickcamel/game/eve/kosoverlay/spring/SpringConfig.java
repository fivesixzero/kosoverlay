/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.spring;

import com.beimin.eveapi.corporation.sheet.CorpSheetResponse;
import com.beimin.eveapi.eve.character.CharacterInfoResponse;
import com.google.common.cache.*;
import com.quickcamel.game.eve.kosoverlay.configuration.*;
import com.quickcamel.game.eve.kosoverlay.service.dto.*;
import com.quickcamel.game.eve.kosoverlay.service.filter.*;
import com.quickcamel.game.eve.kosoverlay.initiator.KeyListener;
import com.quickcamel.game.eve.kosoverlay.service.*;
import com.quickcamel.game.eve.kosoverlay.service.cache.*;
import com.quickcamel.game.eve.kosoverlay.service.rest.*;
import com.quickcamel.game.eve.kosoverlay.service.tasks.*;
import com.quickcamel.game.eve.kosoverlay.service.user.CurrentUserContext;
import com.quickcamel.game.eve.kosoverlay.view.AlwaysOnTopDialogue;
import com.quickcamel.game.eve.kosoverlay.view.ComponentMover;
import com.quickcamel.game.eve.kosoverlay.view.fx.KOSTaskDisplay;
import com.quickcamel.game.eve.kosoverlay.view.fx.controllers.*;
import javafx.collections.*;
import javafx.embed.swing.JFXPanel;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.springframework.context.annotation.*;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ResourceBundleMessageSource;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * The Spring Configuration, configured via code and imported XML
 *
 * @author Louis Burton
 */
@Configuration
//@ImportResource("spring-config.xml")
public class SpringConfig {

    @Bean
    @DependsOn("jfxPanel")
    public KOSBatchService kosBatchService() {
        return new KOSBatchService();
    }

    @Bean
    public IPilotFilter validPilotFilter() {
        return new ValidPilotNamesFilter();
    }

    @Bean
    public IPilotFilter pilotStandingsFilter() {
        return new PilotStandingsFilter();
    }

    @Bean
    public ThreadFactory threadFactory() {
        return new ThreadFactory() {

            private final AtomicInteger m_poolNumber = new AtomicInteger(1);

            @Override
            public Thread newThread(Runnable r) {
                Thread thread = new Thread(r, "KOSCheckTask-" + m_poolNumber.getAndIncrement());
                thread.setDaemon(true);
                return thread;
            }
        };
    }

    @Bean
    public ExecutorService kosCheckExecutorService() {
        return Executors.newFixedThreadPool(Integer.valueOf(configManager().getValue(ConfigKeyConstants.THREADCOUNT, ConfigKeyConstants.THREADCOUNT_DEFAULT)), threadFactory());
    }

    @Bean
    public ExecutorService pilotStandingsFilterExecutorService() {
        return Executors.newFixedThreadPool(Integer.valueOf(configManager().getValue(ConfigKeyConstants.THREADCOUNT, ConfigKeyConstants.THREADCOUNT_DEFAULT)), threadFactory());
    }

    @Bean
    @Scope(value = "prototype")
    public KOSTaskDisplay kosTaskDisplay() {
        return new KOSTaskDisplay();
    }

    @Bean
    @Scope(value = "prototype")
    public KOSCheckTask kosCheckTask() {
        return new KOSCheckAPITask();
    }

    @Bean
    public KeyListener keyListener() {
        return new KeyListener();
    }

    @Bean
    public MessageSourceAccessor messageSourceAccessor() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename(getClass().getPackage().getName() + ".kosoverlay-messages");
        return new MessageSourceAccessor(messageSource);
    }

    @Bean
    public ObservableList<KOSTaskDisplay> resultsList() {
        return FXCollections.observableArrayList();
    }

    @Bean
    public OverlayController overlayController() {
        return new OverlayController();
    }

    @Bean
    public SettingsController settingsController() {
        return new SettingsController();
    }

    @Bean
    public SettingsTabGeneralController settingsTabGeneralController() {
        return new SettingsTabGeneralController();
    }

    @Bean
    public SettingsTabStandingsController settingsTabAPIController() {
        return new SettingsTabStandingsController();
    }

    @Bean
    public SettingsTabSoundsKBController settingsTabSoundsKBController() {
        return new SettingsTabSoundsKBController();
    }

    @Bean
    public SettingsTabAboutController settingsTabAboutController() {
        return new SettingsTabAboutController();
    }

    @Bean
    public HelpController helpController() {
        return new HelpController();
    }

    @Bean
    public ComponentMover componentMover() {
        return new ComponentMover(alwaysOnTopDialogue(), jfxPanel());
    }

    @Bean
    public JFXPanel jfxPanel() {
        return new JFXPanel();
    }

    @Bean
    public AlwaysOnTopDialogue alwaysOnTopDialogue() {
        return new AlwaysOnTopDialogue();
    }

    @Bean
    public IConfigManager configManager() {
        return new PropertiesConfigManager();
    }

    @Bean
    public javax.ws.rs.core.Configuration jerseyClientConfiguration() {
        ClientConfig configuration = new ClientConfig();
        configuration.property(ClientProperties.ASYNC_THREADPOOL_SIZE, Integer.valueOf(configManager().getValue(ConfigKeyConstants.THREADCOUNT, "25")));
        configuration.property(ClientProperties.CONNECT_TIMEOUT, 0);
        configuration.property(ClientProperties.FOLLOW_REDIRECTS, false);
        configuration.register(PlainTextMoxyJsonProvider.class);
        configuration.register(JSONCleaningReaderInterceptor.class);
        return configuration;
    }

    @Bean
    public IRestClient restClient() {
        return new JerseyRestClient(jerseyClientConfiguration());
    }

    @Bean
    public IKOSSummaryHelper statusSummaryBuilder() {
        return new KOSSummaryHelper();
    }

    @Bean
    public IPilotInfoURLBuilder pilotInfoURLBuilder() {
        return new PilotInfoURLBuilder();
    }

    @Bean
    public CurrentUserContext currentUserContext() throws Exception {
        return new CurrentUserContext();
    }

    @Bean
    public CVAPilotLoader cvaPilotLoader() {
        return new CVAPilotLoader();
    }

    @Bean
    public CVACorpLoader cvaCorpLoader() {
        return new CVACorpLoader();
    }

    @Bean
    public CVAAllianceLoader cvaAllianceLoader() {
        return new CVAAllianceLoader();
    }

    @Bean
    public IdToInfoLoader idToInfoLoader() {
        return new IdToInfoLoader();
    }

    @Bean
    public NameToIdLoader nameToIdLoader() {
        return new NameToIdLoader();
    }

    @Bean
    public IdToNameLoader idToNameLoader() {
        return new IdToNameLoader();
    }

    @Bean
    public CorpIdToCorpSheetLoader corpIdToCorpSheetLoader() {
        return new CorpIdToCorpSheetLoader();
    }

    @Bean
    public LoadingCache<String, KOSResultContainerDTO> pilotCVACache() {
        return CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(Long.valueOf(configManager().getValue(ConfigKeyConstants.CACHE_EXPIRY_MINUTES, ConfigKeyConstants.CACHE_EXPIRY_MINUTES_DEFAULT)), TimeUnit.MINUTES)
                .build(cvaPilotLoader());
    }

    @Bean
    public LoadingCache<String, KOSResultContainerDTO> corpCVACache() {
        return CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(Long.valueOf(configManager().getValue(ConfigKeyConstants.CACHE_EXPIRY_MINUTES, ConfigKeyConstants.CACHE_EXPIRY_MINUTES_DEFAULT)), TimeUnit.MINUTES)
                .build(cvaCorpLoader());
    }

    @Bean
    public LoadingCache<String, KOSResultContainerDTO> allianceCVACache() {
        return CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(Long.valueOf(configManager().getValue(ConfigKeyConstants.CACHE_EXPIRY_MINUTES, ConfigKeyConstants.CACHE_EXPIRY_MINUTES_DEFAULT)), TimeUnit.MINUTES)
                .build(cvaAllianceLoader());
    }

    @Bean
    public LoadingCache<String, Long> nameToIdCache() {
        return CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(Long.valueOf(configManager().getValue(ConfigKeyConstants.CACHE_EXPIRY_MINUTES, ConfigKeyConstants.CACHE_EXPIRY_MINUTES_DEFAULT)), TimeUnit.MINUTES)
                .build(nameToIdLoader());
    }

    @Bean
    public LoadingCache<Long, String> idToNameCache() {
        return CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(Long.valueOf(configManager().getValue(ConfigKeyConstants.CACHE_EXPIRY_MINUTES, ConfigKeyConstants.CACHE_EXPIRY_MINUTES_DEFAULT)), TimeUnit.MINUTES)
                .build(idToNameLoader());
    }

    @Bean
    public LoadingCache<Long, CharacterInfoResponse> idToCharacterInfoCache() {
        return CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(Long.valueOf(configManager().getValue(ConfigKeyConstants.CACHE_EXPIRY_MINUTES, ConfigKeyConstants.CACHE_EXPIRY_MINUTES_DEFAULT)), TimeUnit.MINUTES)
                .build(idToInfoLoader());
    }

    @Bean
    public LoadingCache<Long, CorpSheetResponse> corpIdToCorpSheetCache() {
        return CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(Long.valueOf(configManager().getValue(ConfigKeyConstants.CACHE_EXPIRY_MINUTES, ConfigKeyConstants.CACHE_EXPIRY_MINUTES_DEFAULT)), TimeUnit.MINUTES)
                .build(corpIdToCorpSheetLoader());
    }

    @Bean
    public IKOSChecker kosChecker() {
        return new KOSChecker();
    }

    @Bean
    public IBatchPrimer batchPrimer() throws Exception {
        return new CVABatchPrimer();
    }
}
