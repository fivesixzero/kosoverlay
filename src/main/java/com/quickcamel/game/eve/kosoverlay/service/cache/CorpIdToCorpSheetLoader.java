/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.service.cache;

import com.beimin.eveapi.corporation.sheet.CorpSheetParser;
import com.beimin.eveapi.corporation.sheet.CorpSheetResponse;
import com.google.common.cache.CacheLoader;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Load corporation information from the EVE API using a corporation Id
 *
 * @author Louis Burton
 */
public class CorpIdToCorpSheetLoader extends CacheLoader<Long, CorpSheetResponse> {

    private static final Logger m_logger = LoggerFactory.getLogger(CorpIdToCorpSheetLoader.class);

    @Override
    public CorpSheetResponse load(Long corpEveId) throws LoadingException {
        m_logger.debug("Loading Corp Sheet from EVE API for " + corpEveId);
        String errorMsg = "Unable to load Corp Sheet from EVE API for " + corpEveId;
        try {
            CorpSheetResponse response = CorpSheetParser.getInstance().getResponse(corpEveId);
            if (response.hasError()) {
                m_logger.debug(errorMsg);
                if (response.hasError() && response.getError() != null) {
                    m_logger.warn(response.getError().toString());
                }
            }
            else {
                if (m_logger.isDebugEnabled()) {
                    m_logger.debug("Corporation Sheet for corpId " + corpEveId + " : " + ReflectionToStringBuilder.toString(response));
                }
                return response;
            }
        }
        catch (Exception e) {
            m_logger.error(errorMsg, e);
        }
        throw new LoadingException(errorMsg);
    }
}
