/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay;

import com.quickcamel.game.eve.kosoverlay.initiator.KeyListener;
import com.quickcamel.game.eve.kosoverlay.spring.SpringConfig;
import com.quickcamel.game.eve.kosoverlay.spring.SpringFXMLLoader;
import javafx.application.*;
import javafx.concurrent.Task;
import javafx.embed.swing.JFXPanel;
import javafx.scene.*;
import javafx.stage.Stage;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.MessageSourceAccessor;

import javax.swing.*;

/**
 * JavaFX-Launcher for an overlay utility for EVE Online that is able to make Kill On Sight (KOS) checks against pilots
 * This is in accordance with the CVA alliance
 *
 * @author Louis Burton
 */
public class KOSOverlayLauncher extends Application {

    private static final ApplicationContext m_context = new AnnotationConfigApplicationContext(SpringConfig.class);
    private static final SpringFXMLLoader m_loader = new SpringFXMLLoader(m_context);
    private static final Logger m_logger = LoggerFactory.getLogger(KOSOverlayLauncher.class);
    private static final MessageSourceAccessor m_messages = m_context.getBean(MessageSourceAccessor.class);

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        try {
            Parent parentNode = (Parent) m_loader.load("overlay.fxml");
            final KeyListener keyListener = m_context.getBean(KeyListener.class);
            keyListener.registerLauncher(this);

            // Set the scene
            final Scene scene = new Scene(parentNode);
            // Register the hook
            GlobalScreen.registerNativeHook();
            GlobalScreen.getInstance().addNativeKeyListener(m_context.getBean(KeyListener.class));
            startNonFXTask(scene);
        }
        catch (NativeHookException e) {
            m_logger.error(m_messages.getMessage("failed.to.register.native.hook"), e);
            stop();
        }
        catch (Throwable e) {
            e.printStackTrace();
            stop();
            throw e;
        }
    }

    private void startNonFXTask(final Scene scene) {
        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                notifyPreloader(new Preloader.ProgressNotification(0.5));

                // Set the scene
                scene.setFill(null);
                scene.getStylesheets().add(getClass().getResource("styles.css").toExternalForm());

                // Start the overlay
                initialiseAlwaysOnTopDialogue(scene);
                return null;
            }
        };
        new Thread(task).start();
    }

    private void initialiseAlwaysOnTopDialogue(final Scene scene) {
        final JFXPanel fxPanel = m_context.getBean(JFXPanel.class);
        final JDialog jDialog = m_context.getBean(JDialog.class);
        jDialog.add(fxPanel);
        fxPanel.setScene(scene);
        // Let the splash be seen!
        notifyPreloader(new Preloader.ProgressNotification(0.75));
        try {
            Thread.sleep(800);
            notifyPreloader(new Preloader.ProgressNotification(1.00));
            Thread.sleep(200);
        }
        catch (InterruptedException e) {
            m_logger.debug(e.getMessage());
        }
        notifyPreloader(new Preloader.StateChangeNotification(Preloader.StateChangeNotification.Type.BEFORE_START));
        jDialog.setVisible(true);
    }

    @Override
    public void stop() throws Exception {
        m_logger.debug(m_messages.getMessage("stopping"));
        super.stop();
        GlobalScreen.unregisterNativeHook();
        System.runFinalization();
        System.exit(0);
    }
}
