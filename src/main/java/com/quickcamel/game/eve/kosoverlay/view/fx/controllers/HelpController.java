/*
 * Copyright (c) 2014.
 * Author: Louis Burton
 * Hosted: https://bitbucket.org/louisburton/kosoverlay
 *
 * This file is part of KOS Overlay.
 *
 *     KOS Overlay is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     KOS Overlay is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with KOS Overlay.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.quickcamel.game.eve.kosoverlay.view.fx.controllers;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

import javax.inject.Inject;

/**
 * JavaFX Controller for the Help pane
 *
 * @author Louis Burton
 */
public class HelpController {

    public static final String BULLET1_TEXT = "Click and drag the -7- button.";
    public static final String BULLET2_TEXT = "Right click the button & 'Lock Anchor' to fix position.";
    public static final String BULLET3_TEXT = "Click the button to bring back the last results.";
    public static final String BULLET4_TEXT = "Leave the button selected to stop results from fading.";
    public static final String BULLET5_TEXT = "Click the icon on each result for more KOS information.";
    public static final String HELP_TEXT = "\nTo get started :";
    public static final String HELP_STEP_1_TEXT = "Highlight some pilot names from a chat channel within EVE (ie. local!).";
    public static final String HELP_STEP_2_TEXT = "Press and hold 'Ctrl', and hit 'C' twice.";
    public static final String SETTINGS_TEXT = "\nPlease update the settings.\n" +
            "Without these the tool can only detect if someone is KOS from CVA, else must assume they're neutral.";
    public static final String SETTINGS_LINK_TEXT = "Click Here for Settings";

    @FXML
    private Label m_settingsLink;

    @Inject
    private OverlayController m_overlayController;


    @FXML
    public void initialize() {
        m_settingsLink.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                m_overlayController.showSettings();
            }
        });
    }

}
